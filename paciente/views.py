from django.shortcuts import render
from paciente.forms import ModelFormPaciente
# Create your views here.

def registropaciente(request):
    form = ModelFormPaciente(request.POST or None)
    if request.user.is_authenticated():
        titulo = "BIENVENIDO %s" %(request.user)

    contexto = {
        "bienvenido": titulo,
        "formulario_paciente": form,
    }

    if form.is_valid():
        instancia = form.save(commit=False)
        instancia.save()
        print(instancia)
        contexto = {
            "datos": "datos creados correctamente"
        }
    return render(request, "registropaciente.html", contexto)

from django.contrib import admin
from paciente.models import RegistroPaciente
from paciente.forms import ModelFormPaciente
# Register your models here.

class AdminPaciente(admin.ModelAdmin):
    list_display = ["nombre", "rut", "fecha_ingreso", "edad_ingreso","edad_actual"]
    list_filter = ["fecha_ingreso"]
    search_fields = ["nombre", "rut"]
    form = ModelFormPaciente

admin.site.register(RegistroPaciente,AdminPaciente)

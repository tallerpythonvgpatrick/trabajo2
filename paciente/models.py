from django.db import models

# Create your models here.

class RegistroPaciente(models.Model):
    nombre=models.CharField(max_length=20)
    rut=models.CharField(max_length=9)
    edad_ingreso=models.IntegerField()
    edad_actual=models.IntegerField()
    fecha_ingreso=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nombre

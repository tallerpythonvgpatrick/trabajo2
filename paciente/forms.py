from django import forms
from paciente.models import RegistroPaciente


class ModelFormPaciente(forms.ModelForm):
    class Meta:
        model = RegistroPaciente
        fields = ["nombre", "rut", "edad_ingreso", "edad_actual"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre) <= 5 or len(nombre) >= 80:
                raise forms.ValidationError("El campo nombre no puede ser de 5 o menos caracteres o mas de 80 caracteres")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar mas de 5 caracteres")

from django.db import models

# Create your models here.

class RegistroFicha(models.Model):
    enfermedad_detectada=models.IntegerField()
    tratamiento=models.TextField(max_length=200, null=True, blank=True)
    fecha_enfermedad=models.DateTimeField(auto_now_add=True)
    fecha_termino_tratamiento=models.DateTimeField(auto_now=True)
    resultado=models.TextField(max_length=100, null=True, blank=True)

    def __int__(self):
        return self.enfermedad_detectada

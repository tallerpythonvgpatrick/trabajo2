from django import forms
from ficha.models import RegistroFicha


class ModelFormFicha(forms.ModelForm):
    class Meta:
        model = RegistroFicha
        fields = ["enfermedad_detectada", "tratamiento", "resultado"]

    def clean_enfermedad_detectada(self):
        enfermedad_detectada = self.cleaned_data.get("enfermedad_detectada")
        if enfermedad_detectada:
            if enfermedad_detectada <= 0:
                raise forms.ValidationError("el campo enfermedad detectadas no puede ser 0 o numeros negativos")
            return enfermedad_detectada
        else:
            raise forms.ValidationError("debe ingresar valor mayores a 0")

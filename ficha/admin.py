from django.contrib import admin
from ficha.models import RegistroFicha
from ficha.forms import ModelFormFicha
# Register your models here.

class AdminPaciente(admin.ModelAdmin):
    list_display = ["enfermedad_detectada", "tratamiento", "fecha_enfermedad", "fecha_termino_tratamiento", "resultado"]
    list_filter = ["fecha_enfermedad"]
    search_fields = ["enfermedad_detectada", "tratamiento"]
    form=ModelFormFicha

admin.site.register(RegistroFicha,AdminPaciente)

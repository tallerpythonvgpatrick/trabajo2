from django.shortcuts import render
from ficha.forms import ModelFormFicha
# Create your views here.


def registroficha(request):
    form = ModelFormFicha(request.POST or None)
    if request.user.is_authenticated():
        titulo = "BIENVENIDO %s" %(request.user)

    contexto = {
        "bienvenido": titulo,
        "formulario_ficha": form,
    }

    if form.is_valid():
        instancia = form.save(commit=False)
        if not instancia.tratamiento:
            instancia.tratamiento = "SIN DATOS"
        if not instancia.resultado:
            instancia.resultado = "SIN DATOS"
        instancia.save()
        print(instancia)

        contexto = {
            "datos": "datos creados correctamente"
        }
    return render(request, "registroficha.html", contexto)

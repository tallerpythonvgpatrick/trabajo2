from django.contrib import admin
from pediatria.models import RegistroPediatria
from pediatria.forms import ModelFormPediatria
# Register your models here.

class AdminPediatria(admin.ModelAdmin):
    list_display = ["edad"]
    search_fields = ["edad"]
    form=ModelFormPediatria

admin.site.register(RegistroPediatria,AdminPediatria)

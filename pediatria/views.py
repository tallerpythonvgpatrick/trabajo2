from django.shortcuts import render
from pediatria.forms import ModelFormPediatria,FormCorreo
from django.conf import settings
from django.core.mail import send_mail
# Create your views here.

def registropediatria(request):
    form = ModelFormPediatria(request.POST or None)
    if request.user.is_authenticated():
        titulo = "BIENVENIDO %s" %(request.user)

    contexto = {
        "bienvenido": titulo,
        "formulario_pediatria": form,
    }

    if form.is_valid():
        instancia = form.save(commit=False)
        instancia.save()
        print(instancia)
        contexto = {
            "datos": "datos creados correctamente"
        }
    return render(request, "registropediatria.html", contexto)


def correo(request):
    form = FormCorreo(request.POST or None)
    if form.is_valid():
        formulario_nombre = form.cleaned_data.get("nombre")
        formulario_email = form.cleaned_data.get("email")
        formulario_mensaje = form.cleaned_data.get("mensaje")
        asunto = "prueba envio correo"
        email_from = settings.EMAIL_HOST_USER
        email_to = [email_from, "jborquez.r@gmail.com"]
        email_mensaje = "Enviado por %s – Correo: %s – Mensaje: %s" %(formulario_nombre,
        formulario_email, formulario_mensaje)
        send_mail(asunto,
                email_mensaje,
                email_from,
                email_to,
                fail_silently=False
                )

    contexto = {
        "correo": form,
    }
    return render(request, "correo.html",contexto)

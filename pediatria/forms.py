from django import forms
from pediatria.models import RegistroPediatria


class ModelFormPediatria(forms.ModelForm):
    class Meta:
        model = RegistroPediatria
        fields = ["edad"]

    def clean_edad(self):
        edad = self.cleaned_data.get("edad")
        if edad:
            if edad <= 0:
                raise forms.ValidationError("el campo edad no puede ser 0 o numeros negativos")
            return edad
        else:
            raise forms.ValidationError("debe ingresar valor mayores a 0")


class FormCorreo(forms.Form):
    nombre=forms.CharField(required=False)
    email = forms.EmailField()
    mensaje = forms.CharField(widget=forms.Textarea)
